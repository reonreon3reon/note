#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :
#
# Author:   Ashigirl96 
# URL:      http://pwn.hatenablog.com/
# License:  MIT License
# Created:  2015-10-09
#
import numpy as np 
import pylab
import math
from pandas import *
import matplotlib.pyplot as plt
from numpy.random import randn
from fractions import Fraction as F

from IPython.core.display import HTML

class CSS(object):

    def __init__(self):
        self.styles = """<link href="../styles/github.css" rel="stylesheet"></link>"""
    @classmethod
    def css_styling(self):
        return HTML(self.styles)


if __name__ == '__main__':
    CSS.css_styling()
