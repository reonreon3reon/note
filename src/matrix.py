#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :
#
# Author:   Ashigirl96 
# URL:      http://pwn.hatenablog.com/
# License:  MIT License
# Created:  2015-10-02
#
import numpy as np 
import pylab
import math
from pandas import *
import matplotlib.pyplot as plt
from numpy.random import randn
from fractions import Fraction as F

from sympy import init_printing, Matrix, symbols, eye, Rational
from warnings import filterwarnings
init_printing(use_latex = 'mathjax')
filterwarnings('ignore')


